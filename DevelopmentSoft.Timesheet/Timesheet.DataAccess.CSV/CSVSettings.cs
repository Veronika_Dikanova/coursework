﻿namespace Timesheet.DataAccess.CSV
{
    public class CSVSettings
    {
        public CSVSettings(char delimeter, string path)
        {
            Delimeter = delimeter;
            Path = path;
        }

        public char Delimeter { get; }
        public string Path { get; }
    }
}
