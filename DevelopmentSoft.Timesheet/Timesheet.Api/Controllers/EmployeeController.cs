﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Timesheet.App.Services;
using Timesheet.Domain;

namespace Timesheet.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public List<Employee> Show()
        {
            var lastName = (string)HttpContext.Items["LastName"];
            return _employeeService.GetAll(lastName);
        }
    }
}