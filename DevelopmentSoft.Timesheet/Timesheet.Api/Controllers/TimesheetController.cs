﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Timesheet.Domain;
using Timesheet.Domain.Models;

namespace Timesheet.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        private readonly ITimesheetService _timesheetService;

        public TimesheetController(ITimesheetService timesheetService)
        {
            _timesheetService = timesheetService;
        }

        [HttpPost]
        public ActionResult<bool> TrackTime(TimeLog timeLog)
        {
            var lastName = (string)HttpContext.Items["LastName"];
            var validator = new TimeLogValidator(timeLog);

            if (validator.Validate().isValid)
            {
                var result = _timesheetService.TrackTime(timeLog, lastName);
                return result;
            }

            return BadRequest();
        }
    }
}
