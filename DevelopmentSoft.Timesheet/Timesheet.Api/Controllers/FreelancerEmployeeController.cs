﻿using Microsoft.AspNetCore.Mvc;
using Timesheet.Domain;
using Timesheet.Domain.Models;

namespace Timesheet.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FreelancerEmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public FreelancerEmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpPost]
        public ActionResult<bool> Add(FreelancerEmployee freelancerEmployee)
        {
            var lastName = (string)HttpContext.Items["LastName"];

            return _employeeService.Add(lastName, freelancerEmployee) ? true : BadRequest();
        }
    }
}
