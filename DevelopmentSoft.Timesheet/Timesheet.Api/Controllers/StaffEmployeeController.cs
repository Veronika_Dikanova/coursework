﻿using Microsoft.AspNetCore.Mvc;
using Timesheet.Domain;
using Timesheet.Domain.Models;

namespace Timesheet.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StaffEmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public StaffEmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpPost]
        public ActionResult<bool> Add(StaffEmployee staffEmployee)
        {
            var lastName = (string)HttpContext.Items["LastName"];

            return _employeeService.Add(lastName, staffEmployee) ? true : BadRequest();
        }
    }
}
