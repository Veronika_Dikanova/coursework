﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Timesheet.Domain;
using Timesheet.Domain.Models;

namespace Timesheet.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IReportService _reportService;

        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpGet]
        public EmployeeReport Report(EmployeeReport report)
        {
            var lastName = (string)HttpContext.Items["LastName"];

            return _reportService.GetEmployeeReport(lastName, report);
        }
    }
}
