﻿using System.Collections.Generic;
using Timesheet.App.Services;

namespace Timesheet.Domain
{
    public interface IEmployeeService
    {
        bool Add(string lastName, Employee staffEmployee);
        List<Employee> GetAll(string lastName);
    }
}
