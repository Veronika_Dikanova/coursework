﻿using System.Linq;
using Timesheet.App.Services;

namespace Timesheet.Domain.Models
{
    public class StaffEmployee : Employee
    {
        public StaffEmployee(string lastName, decimal salary) 
            : base(lastName, salary, Position.Staff)
        {
        }

        public override decimal CalculateBill(TimeLog[] timeLogs)
        {
            var totalHours = timeLogs.Sum(x => x.WorkingHours);
            decimal bill = 0;
            var workingHoursGroupsByDay = timeLogs.GroupBy(x => x.Date.ToShortDateString());

            foreach (var workingLogsPerDay in workingHoursGroupsByDay)
            {
                int dayHours = workingLogsPerDay.Sum(x => x.WorkingHours);

                if (dayHours > MaxWorkingHoursPerDay)
                {
                    var overtime = dayHours - MaxWorkingHoursPerDay;

                    bill += MaxWorkingHoursPerDay / MaxWorkingHoursPerMonth * Salary;
                    bill += overtime / MaxWorkingHoursPerMonth * Salary * 2;
                }
                else
                {
                    bill += dayHours / MaxWorkingHoursPerMonth * Salary;
                }
            }

            return bill;
        }

        public override string GetPersonalData(char delimeter)
        {
            return $"{LastName}{delimeter}{Salary}{delimeter}Штатный сотрудник{delimeter}\n";
        }

        public override bool CheckInputLog(TimeLog timeLog)
        {
            var isValid = base.CheckInputLog(timeLog);
            isValid = timeLog.LastName == this.LastName && isValid;

            return isValid;
        }
    }
}
