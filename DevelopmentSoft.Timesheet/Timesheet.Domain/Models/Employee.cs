﻿using System;
using Timesheet.Domain.Models;

namespace Timesheet.App.Services
{
    public abstract class Employee
    {
        protected const decimal MaxWorkingHoursPerMonth = 160;
        protected const decimal MaxWorkingHoursPerDay = 8;

        public Employee(string lastName, decimal salary, Position position)
        {
            LastName = lastName;
            Salary = salary;
            Position = position;
        }

        public string LastName { get; set; }
        public decimal Salary { get; set; }

        public Position Position { get; set; }

        public abstract decimal CalculateBill(TimeLog[] timeLogs);
        public abstract string GetPersonalData(char delimeter);

        public virtual bool CheckInputLog(TimeLog timeLog)
        {
            var isValid = timeLog.Date <= DateTime.Now && DateTime.Now.AddYears(-1) <= timeLog.Date;
            
            isValid = timeLog.WorkingHours > 0 && timeLog.WorkingHours <= 24 && !string.IsNullOrWhiteSpace(timeLog.LastName) && isValid;

            return isValid;
        }
    }
}