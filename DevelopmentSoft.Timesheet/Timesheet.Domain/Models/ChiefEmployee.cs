﻿using System;
using System.Linq;
using Timesheet.App.Services;

namespace Timesheet.Domain.Models
{
    [Serializable]
    public class ChiefEmployee : Employee
    {
        public ChiefEmployee(string lastName, decimal salary, decimal bonus) : base(lastName, salary, Position.Chief)
        {
            Bonus = bonus;
        }

        public decimal Bonus { get; set; }

        public override decimal CalculateBill(TimeLog[] timeLogs)
        {
            var totalHours = timeLogs.Sum(x => x.WorkingHours);
            decimal bill = 0;

            var workingHoursGroupsByDay = timeLogs.GroupBy(x => x.Date.ToShortDateString());

            foreach (var workingLogsPerDay in workingHoursGroupsByDay)
            {
                int dayHours = workingLogsPerDay.Sum(x => x.WorkingHours);

                if (dayHours > MaxWorkingHoursPerDay)
                {
                    bill += MaxWorkingHoursPerDay / MaxWorkingHoursPerMonth * (Salary + Bonus);
                }
                else
                {
                    bill += dayHours / MaxWorkingHoursPerMonth * Salary;
                }
            }

            return bill;
        }

        public override string GetPersonalData(char delimeter)
        {
            return $"{LastName}{delimeter}{Salary}{delimeter}Руководитель{delimeter}{Bonus}{delimeter}\n";
        }
    }
}
