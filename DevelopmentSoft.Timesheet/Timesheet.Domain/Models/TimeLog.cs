﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Timesheet.Domain.Models
{
    public class TimeLog
    {
        public DateTime Date { get; set; }
        public int WorkingHours { get; set; }        
        public string LastName { get; set; }
        public string Comment { get; set; }
    }

    public class TimeLogValidator
    {
        private readonly TimeLog _timeLog;

        public TimeLogValidator(TimeLog timeLog)
        {
            _timeLog = timeLog;
        }
        public (bool isValid, IEnumerable<ValidationResult> validationResults) Validate()
        {
            var validationResults = new List<ValidationResult>();

            var validationContext = new ValidationContext(this);
            var isValid = Validator.TryValidateObject(this, validationContext, validationResults);

            isValid = _timeLog.Date <= DateTime.Now && DateTime.Now.AddYears(-1) <= _timeLog.Date && _timeLog.WorkingHours > 0 && _timeLog.WorkingHours <= 24 &&
                !string.IsNullOrWhiteSpace(_timeLog.LastName) && isValid;

            return (isValid, validationResults);
        }
    }
}
