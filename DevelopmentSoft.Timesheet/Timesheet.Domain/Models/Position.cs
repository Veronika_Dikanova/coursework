﻿namespace Timesheet.Domain.Models
{
    public enum Position
    {
        Chief,
        Staff,
        Freelancer
    }
}
