﻿using System;
using System.Collections.Generic;

namespace Timesheet.Domain.Models
{
    public class EmployeeReport
    {
        public EmployeeReport()
        {
            TimeLogs = new List<TimeLog>();
        }

        public string LastName { get; set; }
        public DateTime StartDate { get; set; } = DateTime.Now.AddYears(-1);
        public DateTime EndDate { get; set; } = DateTime.Now;

        public List<TimeLog> TimeLogs { get; set; }

        public decimal TotalHours { get; set; }
        public decimal Bill { get; set; }
    }
}
