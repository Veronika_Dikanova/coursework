﻿namespace Timesheet.Domain
{
    public interface IAuthService
    {
        string Login(string lastName, string secret);
    }
}