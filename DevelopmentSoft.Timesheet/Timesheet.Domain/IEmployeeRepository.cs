﻿using System.Collections.Generic;

namespace Timesheet.App.Services
{
    public interface IEmployeeRepository
    {
        void Add(Employee staffEmployee);
        Employee Get(string lastName);
        List<Employee> GetAll();
    }
}