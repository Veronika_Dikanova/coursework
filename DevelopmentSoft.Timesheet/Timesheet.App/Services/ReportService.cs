﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timesheet.Domain;
using Timesheet.Domain.Models;

namespace Timesheet.App.Services
{
    public class ReportService : IReportService
    {
        private const decimal MaxWorkingHoursPerMonth = 160m;
        private const decimal MaxWorkingHoursPerDay = 8m;

        private readonly ITimesheetRepository _timesheetRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public ReportService(ITimesheetRepository timesheetRepository, IEmployeeRepository employeeRepository)
        {
            _timesheetRepository = timesheetRepository;
            _employeeRepository = employeeRepository;
        }

        public EmployeeReport GetEmployeeReport(string lastName, EmployeeReport report)
        {
            string position = _employeeRepository.Get(lastName).Position.ToString();

            var employee = _employeeRepository.Get(report.LastName);

            if (employee == null || (!Position.Chief.ToString().Equals(position) && !lastName.Equals(report.LastName)))
            {
                return null;
            }

            var timeLogs = _timesheetRepository.GetTimeLogs(employee.LastName).Where(s => report.StartDate <= s.Date && s.Date <= report.EndDate).ToArray();

            if (timeLogs == null || timeLogs.Length == 0)
            {
                return new EmployeeReport
                {
                    LastName = employee.LastName
                };
            }

            var totalHours = timeLogs.Sum(x => x.WorkingHours);
            var bill = employee.CalculateBill(timeLogs);

            return new EmployeeReport()
            {
                LastName = employee.LastName,
                TimeLogs = timeLogs.ToList(),
                Bill = bill,
                TotalHours = totalHours,
                StartDate = timeLogs.Select(t => t.Date).Min(),
                EndDate = timeLogs.Select(t => t.Date).Max()
            };
        }
    }
}
