﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Timesheet.App.Services;
using Timesheet.Domain;
using Timesheet.Domain.Models;

namespace Timesheet.Tests
{
    public class EmployeeService : IEmployeeService
    {
        private IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public bool Add(string lastName, Employee employee)
        {
            string position = _employeeRepository.Get(lastName).Position.ToString();

            bool employeeExists = _employeeRepository.Get(employee.LastName) != null;
            
            bool isValid = !string.IsNullOrEmpty(employee.LastName) && employee.Salary > 0 && 
                                    Position.Chief.ToString().Equals(position) &&
                                    (employee as ChiefEmployee).Bonus > 0 && !employeeExists;
            
            if (isValid)
            {
                _employeeRepository.Add(employee);
            }
            
            return isValid;
        }

        public List<Employee> GetAll(string lastName)
        {
            string position = _employeeRepository.Get(lastName).Position.ToString();
            List<Employee> employees = new();
            
            if (Position.Chief.ToString().Equals(position))
            {
                employees = _employeeRepository.GetAll();
            }

            return employees;
        }
    }
}