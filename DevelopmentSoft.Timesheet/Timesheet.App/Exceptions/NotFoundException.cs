﻿using System;

namespace Timesheet.App.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        { 
        }
    }
}
